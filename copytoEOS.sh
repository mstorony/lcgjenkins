#!/bin/bash -x

export EOS_MGM_URL=root://eosuser.cern.ch
export PLATFORM=`$WORKSPACE/lcgjenkins/getPlatform.py`
FILES=*.tgz
weekday=`date +%a`
txtfile=LCG_${LCG_VERSION}_$PLATFORM.txt
isDone=isDone-$PLATFORM
isDoneUnstable=isDone-unstable-$PLATFORM

if [ "${BUILDMODE}" == "nightly" ]; then
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/nightlies/$LCG_VERSION/$weekday
else
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/releases
fi

cd $WORKSPACE/build

if [[ $PLATFORM == *slc6* || $PLATFORM == *cc7* || $PLATFORM == *centos7* ]]; then

    if [ "${BUILDMODE}" == "nightly" ]; then 
	eos rm $basespace/$isDone 
	eos rm $basespace/$isDoneUnstable
        xrdcp $isDone root://eosuser.cern.ch/$basespace/$isDone 
        xrdcp $isDoneUnstable root://eosuser.cern.ch/$basespace/$isDoneUnstable
    fi
    eos rm $basespace/$txtfile
    xrdcp $txtfile root://eosuser.cern.ch/$basespace/$txtfile
    
    if [ "$(ls -A tarfiles)" ]; then
	echo "Take action: tarfiles is not Empty"
	cd tarfiles
	for files in $FILES
	do
	    if [ "${BUILDMODE}" == "nightly" ]; then
		xrdcp -f $files root://eosuser.cern.ch/$basespace/$files
	    else
		ls $basespace/$files
		if [ $? == 0 ]; then
		    echo "The file already exists in EOS"
		else
                    xrdcp $files root://eosuser.cern.ch/$basespace/$files
		fi
            fi
	done
	
    else
	echo "tarfiles is Empty. I will assume this is not an error though"
        exit 0
    fi
    
else
    if [ "${BUILDMODE}" == "nightly" ]; then
	ssh sftnight@lcgapp-slc6-physical2.cern.ch 'rm -f $basespace/$isDone'
	ssh sftnight@lcgapp-slc6-physical2.cern.ch 'rm -f $basespace/$isDoneUnstable'
        scp $isDone $isDoneUnstable sftnight@lcgapp-slc6-physical2.cern.ch:$basespace
    fi	
    ssh sftnight@lcgapp-slc6-physical2.cern.ch 'rm -f $basespace/$txtfile'
    scp $txtfile sftnight@lcgapp-slc6-physical2.cern.ch:$basespace

    if [ "$(ls -A tarfiles)" ]; then
	echo "Take action: tarfiles is not Empty"
	cd tarfiles
	scp *tgz sftnight@lcgapp-slc6-physical2.cern.ch:$basespace
    else
	echo "tarfiles is Empty. I will assume this is not an error though"
        exit 0
    fi
fi
