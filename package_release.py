#!/usr/bin/env python

import os, sys
import Tools
import common_parameters
from optparse import OptionParser
##from common_parameters import RPM_SCRIPTDIR
from copyRPMS import copyRPMS
from getNewRPMrevision import get_new_RPM_revision

def patchLCGCMT(lcg_version):
    #adjust LCG_Settings/cmt/requirements
    command = 'sed -i "s#/../../..#/../../../LCG_%s#g" LCGCMT/LCGCMT_%s/LCG_Settings/cmt/requirements' %(lcg_version,lcg_version)
    Tools.executeAndCheckCommand(command,"patching LCGCMT_version","ERROR patching LCGCMT")
    command = 'sed -i "s#/LCG_\$(LCG_config_version)#/LCG_%s#g" LCGCMT/LCGCMT_%s/LCG_Settings/cmt/requirements' %(lcg_version,lcg_version)
    Tools.executeAndCheckCommand(command,"patching LCGCMT_config_version","ERROR patching LCGCMT")
    command = 'sed -i "/LCG_nightlies/d" LCGCMT/LCGCMT_%s/LCG_Configuration/cmt/requirements' %lcg_version
    Tools.executeAndCheckCommand(command,"removing LCG_nightlies tag","ERROR patching LCGCMT")

def extractLCGSummary(scriptdir,platform,lcg_version, policy):
    if policy == "release":
      command = "%s/lcgjenkins/extract_LCG_summary.py . %s %s RELEASE" %(scriptdir, platform, lcg_version)
    else:
      command = "%s/lcgjenkins/extract_LCG_summary.py . %s %s UPGRADE" %(scriptdir, platform, lcg_version)
    Tools.executeAndCheckCommand(command,"Extracting LCG summary","ERROR extracting LCG summary")

def createSpecFiles(workdir,platform,rpm_revision):
    command = "/afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/package/createLCGRPMSpec.py LCG_externals_%s.txt -b %s/packaging -o externals.spec --release %s" %(platform, workdir, rpm_revision)
    Tools.executeAndCheckCommand(command,"creating specfile for externals","ERROR creating specfile for externals")
    command = "/afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/package/createLCGRPMSpec.py LCG_generators_%s.txt -b %s/packaging -o generators.spec --release %s" %(platform, workdir, rpm_revision)
    Tools.executeAndCheckCommand(command,"creating specfile for generators","ERROR creating specfile for generators")

def buildRPMs():
    command = "rpmbuild -bb externals.spec"
    Tools.executeAndCheckCommand(command,"building externals RPMs", "ERROR building externals RPMs")
    command = "rpmbuild -bb generators.spec"
    Tools.executeAndCheckCommand(command,"building generators RPMs", "ERROR generators RPMs")

##########################
if __name__ == "__main__":

  # extract command line parameters
  usage = "usage: %prog scriptdir workdir lcg_version platform target policy"
  parser = OptionParser(usage)
  (options, args) = parser.parse_args()
  if len(args) != 6:
    parser.error("incorrect number of arguments.")
  else:
    scriptdir     =  args[0]
    workdir       =  args[1]
    lcg_version   =  args[2]
    platform      =  args[3]
    target        =  args[4]
    policy        =  args[5]

  if policy not in ("release", "Generators", "Rebuild"):
    print "ERROR: unknown install/copy policy"
    sys.exit(1)

  tmparea = common_parameters.tmparea
  # do the job
  if target in ["all", "generators"]:
    patchLCGCMT(lcg_version)
####  extractLCGSummary(scriptdir,platform,lcg_version, policy)
  if policy in ("Generators", "Rebuild"):
    rpm_revision = get_new_RPM_revision(lcg_version, platform)
  else:
    rev_number = os.environ['RPM_REVISION_NUMBER'] if 'RPM_REVISION_NUMBER' in os.environ else 'a'
    if rev_number.isdigit():
      rpm_revision = rev_number
    else:
      rpm_revision = Tools.extractLCGNumber(lcg_version)
  createSpecFiles(workdir,platform,rpm_revision)
  buildRPMs()
  copyRPMS(workdir, tmparea, target=target, policy=policy)
