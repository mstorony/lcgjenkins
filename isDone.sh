#!/bin/bash
# The following variables are expected:
#  SLOTNAME:  Nightly slot name
#  LABEL:     Worker node label (e.g. slc6)
#  COMPILER:  Compiler and version
#  BUILDTYPE: Build type (e.g. Release)
#  WORKSPACE: Directory where the job is running in the local machine
isdone=$1

arch=`uname -p`
this=$(dirname $0)

platform=`$this/getPlatform.py`

today=$(date +%a)
nightdir=/afs/cern.ch/sw/lcg/app/nightlies
donefile=$nightdir/$SLOTNAME/$today/isDone-$platform
unstabledonefile=$nightdir/$SLOTNAME/$today/isDone-unstable-$platform
if [ $isdone == 1 ]; then
#  touch $donefile
  touch $WORKSPACE/build/isDone-$platform
  echo "Created file $donefile"
elif [ $isdone == 2 ]; then
#  touch $unstabledonefile
  touch $WORKSPACE/build/isDone-unstable-$platform
  echo "Created unstable file $unstabledonefile"
else
  rm -f $donefile
  rm -f $unstabledonefile
  rm -f $WORKSPACE/build/isDone-$platform
  rm -f $WORKSPACE/build/isDone-unstable-$platform
  echo "Removed file $donefile and $unstabledonefile"
fi
