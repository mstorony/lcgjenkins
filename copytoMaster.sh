#!/bin/bash -x

# This script is intended to copy results of builds to the Master node

if [ $# -ge 1 ]; then
  SLOTNAME=$1; shift
else
  echo "$0: expecting 1 arguments: [lcg_version]"
  return
fi

export SLOTNAME
weekday=`date +%a`

kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab

if [ "${BUILDMODE}" == "nightly" ]; then
    masterspace=/build/workspace/nightlies-tarfiles/$SLOTNAME/$weekday/
else
    masterspace=/build/workspace/releases-tarfiles/
fi

cd $WORKSPACE/build

if [ "${BUILDMODE}" == "nightly" ]; then 
    scp isDone* sftnight@phsft-jenkins.cern.ch:$masterspace 
fi
scp *.txt sftnight@phsft-jenkins.cern.ch:$masterspace

if [ "$(ls -A tarfiles)" ]; then
    echo "Take action: tarfiles is not Empty"
    cd tarfiles
    scp *.tgz sftnight@phsft-jenkins.cern.ch:$masterspace
else
    echo "tarfiles directory is empty. I will assume this is not an error though"
    exit 0
fi


