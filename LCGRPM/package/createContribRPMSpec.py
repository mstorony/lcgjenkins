#!/usr/bin/env python
"""
Script to create the LCGCMT RPM Spec file based on the JSON configuration
produced by the build itself.
See usage method and command help for use.

"""
import copy
import logging
import os
import optparse
import re
import sys
from string import Template

log = logging.getLogger()
log.setLevel(logging.INFO)
CONTRIBDIR="/opt/lcg"

###############################################################################
# Class to build the SPEC itself
###############################################################################

class ContribRpmSpec(object):
    """ Class presenting the whole spec """
    def __init__(self, name, version, platform, lcgdir, rpmroot):
        """ Initialize with the list of RPMs """
        self.name = name
        self.version = version
        self.platform = platform
        self.lcgdir = lcgdir
        self.rpmroot = rpmroot
        
        # Building the build dir paths
        myroot =  rpmroot
        self.topdir = "%s/rpmbuild" % myroot
        self.tmpdir = "%s/tmpbuild" % myroot
        self.rpmtmp = "%s/tmp" % myroot
        self.srcdir = os.path.join(self.topdir, "SOURCES")
        self.rpmsdir =  os.path.join(self.topdir, "RPMS")
        self.srpmsdir =  os.path.join(self.topdir, "SRPMS")
        self.builddir =  os.path.join(self.topdir, "BUILD")

        # And creating them if needed
        for d in [self.srcdir, self.rpmsdir, self.srpmsdir, self.builddir]:
            if not os.path.exists(d):
                os.makedirs(d)

        self.buildroot = os.path.join(self.tmpdir, "%s-%s-%s-buildroot" % \
                                      (self.name, self.version, self.platform))
        if not os.path.exists(self.buildroot):
            os.makedirs(self.buildroot)

    def getContribSourceDir(self):
        """ Return the source directory for the contrib (where to copy from) """
        return os.path.join(self.lcgdir, self.name, self.version, self.platform)

    def getContribDir(self):
        """ Return the directory for the contrib in the install area"""
        return os.path.join(CONTRIBDIR, self.name, self.version, self.platform)

    
    def getHeader(self):
        """ Build the SPEC Header """
        rpm_header = Template("""
%define contribName $name
%define contribVersion $version
%define contribPlatform $platform
%define contribPlatformFixed $platformFixed
%define contribDir $contribDir
%define CONTRIBROOT $lcgdir

%define _topdir $topdir
%define tmpdir $tmpdir
%define _tmppath $rpmtmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: %{contribName}_%{contribVersion}_%{contribPlatformFixed}
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: Contrib %{contribName} %{contribVersion} %{contribPlatform}
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/%{contribName}-%{contribVersion}-%{contribPlatform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{contribName}_%{contribVersion}_%{contribPlatformFixed}

""").substitute(name=self.name,
                version=self.version,
                platform=self.platform,
                platformFixed=self.platform.replace("-","_"),
                lcgdir=self.lcgdir,
                topdir=self.topdir,
                tmpdir=self.tmpdir,
                rpmtmp=self.rpmtmp,
                contribDir=CONTRIBDIR)
        return rpm_header

# RPM requirements for the whole package
#############################################################

    def getRequires(self):
        rpm_requires = ""
        return rpm_requires

# RPM Description section
#############################################################

    def getDescriptions(self):
        rpm_desc = """
%description
%{contribName} %{contribVersion}

"""
        return rpm_desc

# RPM Common section with build
#############################################################

    def getCommon(self):
        rpm_common = """
%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{contribDir}
if [ $? -ne 0 ]; then
  exit $?
fi

cd ${RPM_BUILD_ROOT}%{contribDir}
mkdir -p  ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}
rsync -arz %{CONTRIBROOT}/%{contribName}/%{contribVersion}/%{contribPlatform}/ ${RPM_BUILD_ROOT}%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}

%files
%defattr(-,root,root)
%{contribDir}/%{contribName}/%{contribVersion}/%{contribPlatform}

"""

        rpm_common += """

%post

%postun

%clean
"""
        return rpm_common

# RPM Trailer
#############################################################

    def getTrailer(self):
        rpm_trailer = """
%define date    %(echo `LC_ALL=\"C\" date +\"%a %b %d %Y\"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- first Version
"""
        return rpm_trailer


# Get the whole spec...
#############################################################

    def getSpec(self):
        """ Concatenate all the fragments """

        rpm_global = self.getHeader() \
        + self.getRequires() \
        + self.getDescriptions() \
        + self.getCommon() \
        + self.getTrailer()

        return rpm_global




# Main method
#############################################################
def usage(cmd):
    """ Prints out how to use the script... """
    cmd = os.path.basename(cmd)
    return """\n%(cmd)s [options] name version platform

Prepare the SPEC file for a contrib release
Call with --help for details
""" % { "cmd" : cmd }


###############################################################################
# Main method
###############################################################################
if __name__ == '__main__':
    # Setting logging 
    logging.basicConfig(stream=sys.stderr)

    # Parsing options
    parser = optparse.OptionParser(usage=usage(sys.argv[0]))
    parser.add_option('-d', '--debug',
                      dest="debug",
                      default=False,
                      action="store_true",
                      help="Show debug information")
    parser.add_option('-l', '--lcgdir',
                      dest="lcgdir",
                      default="/afs/cern.ch/sw/lcg/contrib",
                      action="store",
                      help="Force LCG dir if different from the one containing the config file")
    parser.add_option('-b', '--buildroot',
                      dest="buildroot",
                      default="/tmp",
                      action="store",
                      help="Force build root")
    parser.add_option('-o', '--output',
                      dest="output",
                      default = None,
                      action="store",
                      help="File name for the generated specfile [default output to stdout]")

    opts, args = parser.parse_args(sys.argv)

    if len(args) != 4:
        parser.error("Please specify <name> <version> <platform>")

    if opts.debug:
        log.setLevel(logging.DEBUG)
        
    contribName = args[1]
    contribVersion = args[2]
    contribPlatform = args[3]
    
    # The build root (default /tmp)
    rpmroot = opts.buildroot

    # CHeck if the contrib can be found
    lcgdir = ""
    if opts.lcgdir != None:
        lcgdir = opts.lcgdir

    contribDir = os.path.join(lcgdir, contribName, contribVersion, contribPlatform)
    if not os.path.exists(contribDir):
        raise Exception("Cound not find contrib in %s" % contribDir)

    log.info("Processing %s %s %s" % (contribName, contribVersion, contribPlatform))
    log.info("Contrib dir: %s" % contribDir)
    log.info("Build root: %s" % rpmroot)

        
    spec = ContribRpmSpec(contribName, contribVersion, contribPlatform, lcgdir, rpmroot)

    if opts.output:
        with open(opts.output, "w") as outputfile:
            outputfile.write(spec.getSpec())
    else:
        print spec.getSpec()
    log.info("Spec file generated")

