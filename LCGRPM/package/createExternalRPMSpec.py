#!/usr/bin/env python
"""

Script to package classic external from a directory (typically on AFS).
Needed for middleware and some other packages.


"""
import copy
import logging
import os
import optparse
import re
import sys
from string import Template

log = logging.getLogger()
log.setLevel(logging.INFO)
EXTDIR="/opt/lcg"

###############################################################################
# Class to build the SPEC itself
###############################################################################

class ExtRpmSpec(object):
    """ Class presenting the whole spec """
    def __init__(self, name, version, platform, exttype, subdir, lcgdir, rpmroot):
        """ Initialize with the list of RPMs """
        self.name = name
        self.version = version
        self.platform = platform
        self.exttype = exttype
        self.subdir = subdir
        self.lcgdir = lcgdir
        self.rpmroot = rpmroot
        
        # Building the build dir paths
        myroot =  rpmroot
        self.topdir = "%s/rpmbuild" % myroot
        self.tmpdir = "%s/tmpbuild" % myroot
        self.rpmtmp = "%s/tmp" % myroot
        self.srcdir = os.path.join(self.topdir, "SOURCES")
        self.rpmsdir =  os.path.join(self.topdir, "RPMS")
        self.srpmsdir =  os.path.join(self.topdir, "SRPMS")
        self.builddir =  os.path.join(self.topdir, "BUILD")

        # And creating them if needed
        for d in [self.srcdir, self.rpmsdir, self.srpmsdir, self.builddir]:
            if not os.path.exists(d):
                os.makedirs(d)

        self.buildroot = os.path.join(self.tmpdir, "%s-%s-%s-buildroot" % \
                                      (self.name, self.version, self.platform))
        if not os.path.exists(self.buildroot):
            os.makedirs(self.buildroot)

    def getExtSourceDir(self):
        """ Return the source directory for the ext (where to copy from) """
        return os.path.join(*[ p for p in [ self.lcgdir, self.exttype, self.subdir, self.name,
                                           self.version, self.platform] if p != None])

    def getExtDir(self):
        """ Return the directory for the ext in the install area"""
        return os.path.join(*[ p for p in [ EXTDIR, self.exttype, self.subdir,
                                           self.name, self.version, self.platform ] if p != None])

    
    def getHeader(self):
        """ Build the SPEC Header """
        rpm_header = Template("""
%define extName $name
%define extVersion $version
%define extPlatform $platform
%define extPlatformFixed $platformFixed
%define extDir $extDir
%define EXTROOT $lcgdir
%define sourceDir $sourceDir
%define targetDir $targetDir

%define _topdir $topdir
%define tmpdir $tmpdir
%define _tmppath $rpmtmp
%define debug_package %{nil}
%global __os_install_post /usr/lib/rpm/check-buildroot

Name: %{extName}_%{extVersion}_%{extPlatformFixed}
Version: 1.0.0
Release: 1
Vendor: LHCb
Summary: Ext %{extName} %{extVersion} %{extPlatform}
License: GPL
Group: LCG
BuildRoot: %{tmpdir}/%{extName}-%{extVersion}-%{extPlatform}-buildroot
BuildArch: noarch
AutoReqProv: no
Prefix: /opt/lcg
Provides: /bin/sh
Provides: %{extName}_%{extVersion}_%{extPlatformFixed}

""").substitute(name=self.name,
                version=self.version.replace("-", "_"),
                platform=self.platform,
                platformFixed=self.platform.replace("-","_"),
                lcgdir=self.lcgdir,
                topdir=self.topdir,
                tmpdir=self.tmpdir,
                rpmtmp=self.rpmtmp,
                extDir=EXTDIR,
                sourceDir=self.getExtSourceDir(),
                targetDir=self.getExtDir())
        return rpm_header

# RPM requirements for the whole package
#############################################################

    def getRequires(self):
        rpm_requires = ""
        return rpm_requires

# RPM Description section
#############################################################

    def getDescriptions(self):
        rpm_desc = """
%description
%{extName} %{extVersion}

"""
        return rpm_desc

# RPM Common section with build
#############################################################

    def getCommon(self):
        rpm_common = """
%prep

%build

%install

cd %_topdir/SOURCES

[ -d ${RPM_BUILD_ROOT} ] && rm -rf ${RPM_BUILD_ROOT}

/bin/mkdir -p ${RPM_BUILD_ROOT}%{extDir}
if [ $? -ne 0 ]; then
  exit $?
fi

cd ${RPM_BUILD_ROOT}%{extDir}
mkdir -p  ${RPM_BUILD_ROOT}%{targetDir}
rsync -arz %{sourceDir}/ ${RPM_BUILD_ROOT}%{targetDir}

%files
%defattr(-,root,root)
%{targetDir}

"""

        rpm_common += """

%post

%postun

%clean
"""
        return rpm_common

# RPM Trailer
#############################################################

    def getTrailer(self):
        rpm_trailer = """
%define date    %(echo `LC_ALL=\"C\" date +\"%a %b %d %Y\"`)

%changelog

* %{date} User <ben.couturier..rcern.ch>
- Packaged external
"""
        return rpm_trailer


# Get the whole spec...
#############################################################

    def getSpec(self):
        """ Concatenate all the fragments """

        rpm_global = self.getHeader() \
        + self.getRequires() \
        + self.getDescriptions() \
        + self.getCommon() \
        + self.getTrailer()

        return rpm_global




# Main method
#############################################################
def usage(cmd):
    """ Prints out how to use the script... """
    cmd = os.path.basename(cmd)
    return """\n%(cmd)s [options] name version platform

Prepare the SPEC file for a ext release
Call with --help for details
""" % { "cmd" : cmd }


###############################################################################
# Main method
###############################################################################
if __name__ == '__main__':
    # Setting logging 
    logging.basicConfig(stream=sys.stderr)

    # Parsing options
    parser = optparse.OptionParser(usage=usage(sys.argv[0]))
    parser.add_option('-d', '--debug',
                      dest="debug",
                      default=False,
                      action="store_true",
                      help="Show debug information")
    parser.add_option('-l', '--lcgdir',
                      dest="lcgdir",
                      default="/afs/cern.ch/sw/lcg",
                      action="store",
                      help="Force LCG dir if different from the one containing the config file")
    parser.add_option('-b', '--buildroot',
                      dest="buildroot",
                      default="/tmp",
                      action="store",
                      help="Force build root")
    parser.add_option('-t', '--type',
                      dest="exttype",
                      default="external",
                      action="store",
                      help="Type of the packaged file: external, app/releases")
    parser.add_option('-s', '--subdir',
                      dest="subdir",
                      default=None,
                      action="store",
                      help="Subdirectory in which to find the external (e.g. Grid, for the Grid ones...)")
    parser.add_option('-o', '--output',
                      dest="output",
                      default = None,
                      action="store",
                      help="File name for the generated specfile [default output to stdout]")
    opts, args = parser.parse_args(sys.argv)

    if len(args) != 4:
        parser.error("Please specify <name> <version> <platform>")

    if opts.debug:
        log.setLevel(logging.DEBUG)
        
    extName = args[1]
    extVersion = args[2]
    extPlatform = args[3]
    
    # The build root (default /tmp)
    rpmroot = opts.buildroot

    # Check if the ext can be found
    lcgdir = ""
    if opts.lcgdir != None:
        lcgdir = opts.lcgdir

    extDir = os.path.join(*[ p for p in [ lcgdir, opts.exttype, opts.subdir, extName,
                                         extVersion, extPlatform] if p != None])
    print extDir
    if not os.path.exists(extDir):
        raise Exception("Cound not find ext in %s" % extDir)

    log.info("Processing %s %s %s" % (extName, extVersion, extPlatform))
    log.info("Type      : %s" % opts.exttype)
    log.info("Subdir    : %s" % opts.subdir)
    log.info("Ext dir   : %s" % extDir)
    log.info("Build root: %s" % rpmroot)
        
    spec = ExtRpmSpec(extName, extVersion, extPlatform, opts.exttype, opts.subdir, lcgdir, rpmroot)

    if opts.output:
        with open(opts.output, "w") as outputfile:
            outputfile.write(spec.getSpec())
    else:
        print spec.getSpec()
    log.info("Spec file generated")

